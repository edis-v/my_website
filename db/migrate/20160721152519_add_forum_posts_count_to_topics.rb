class AddForumPostsCountToTopics < ActiveRecord::Migration

  def up
    add_column :topics, :forum_posts_count, :integer, default: 0
    Topic.reset_column_information
    Topic.all.each do |topic|
      topic.update_attribute(:forum_posts_count, topic.forum_posts.length)
    end
  end

  def down
    remove_column :topics, :forum_posts_count
  end
end
