# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

[Image, ForumPost, Comment, Post, Topic, User, Category].each(&:delete_all)

categories = %w[Sports Politics Showbiz Fun Auto-Moto Football Movies News
              Lifestyle Columns Weather World Europe Famous Other]

15.times do |n|
  Category.create(name: categories[n], priority: n + 1)
end

90.times {
  User.create(first_name: FFaker::Name.first_name,
              last_name: FFaker::Name.last_name,
              email: FFaker::Internet.email,
              password: FFaker::Internet.password(min=8),
              role: rand(0..2)
  )
}

User.create(first_name: FFaker::Name.first_name,
            last_name: FFaker::Name.last_name,
            email: 'guest@mail.com',
            password: 'guestguest123',
            role: 0
)
User.create(first_name: FFaker::Name.first_name,
            last_name: FFaker::Name.last_name,
            email: 'moderator@mail.com',
            password: 'moderator123',
            role: 1
)
User.create(first_name: FFaker::Name.first_name,
            last_name: FFaker::Name.last_name,
            email: 'admin@mail.com',
            password: 'adminadmin123',
            role: 2
)



10.times {
  Category.all.each do |category|
    Post.create(title: FFaker::Lorem.sentence(rand(3..8)),
                content: FFaker::Lorem.sentences(rand(15..60)),
                user_id: User.order('RANDOM()').first.id,
                category_id: category.id
    )
  end
}

url = File.join(Rails.root, 'db', 'images.txt')
file = File.open(url, 'r')
Post.all.each do |post|
  Image.create(image: file.readline, post_id: post.id) unless file.eof?
end
while !file.eof?
  Image.create(image: file.readline, post_id: Post.order('RANDOM()').first.id)
end

10.times {
  Category.all.each do |category|
    Topic.create(name: FFaker::Lorem.sentence(rand(2..8)),
                 category_id: category.id
    )
  end

}

rand(20..60).times {
  Post.all.each do |post|
    Comment.create(content: FFaker::Lorem.sentences(rand(1..10)),
                   user_id: User.order('RANDOM()').first.id,
                   post_id: post.id
    )
  end
}

rand(5..25).times {
  Topic.all.each do |topic|
    ForumPost.create(content: FFaker::Lorem.sentences(rand(1..10)),
                     user_id: User.order('RANDOM()').first.id,
                     topic_id: topic.id
    )
  end

}

puts 'Seeding complete'