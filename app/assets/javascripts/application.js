// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require ckeditor/init
//= require cloudinary
//= require html.sortable.min
//= require bootstrap-sprockets
//= require social-share-button
//= require fancybox
//= require turbolinks
//= require_tree .


$(document).on('ready page:load', function(){

    $(document).on('keyup','#query', function(e){
        console.log("Clicked", $(e.target).val());
        $('#link_search').attr('href','/search?query='+ $(e.target).val());
    });

    $(document).on('click', '#link_search', function(e){
        if($('#query').val()==''){
            e.preventDefault();
        }
    });

    $(document).on('click', 'ul.main_nav li', function(e){
        $(this).children().addClass('active');
    });

});