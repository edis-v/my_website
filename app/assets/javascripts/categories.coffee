# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'ready page:load', ->


  $(document).on 'click', 'div#flash_notice span', ->
    $(this).parent().remove()

  set_positions = ->
    $('.category_sort').each (i) ->
      $(this).attr("data-pos",i+1);

  sort = ->
    console.log 'calling sort'
    set_positions()
    sortable '.sortable'

    sortable('.sortable')[0].addEventListener 'sortupdate', (e) ->
      updated_order = []
      $('.category_sort').each (i) ->
        updated_order.push({ id: $(this).data("id"), position: i+1 })
      $.ajax
        type: 'PUT'
        url: '/categories/sort',
        data: order: updated_order

#  $(document).ajaxSuccess sort

  $('form#new_category').bind 'ajax:complete', sort





