# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'ready page:load', ->

  $(document).on 'click','button#search_button', ->
    url = '';
    q = $('input#q').val();
    if q
      if $('input:checked').length
        url = $('input:checked').val();
        url += '&q=' + q;
      else
        url = '/admin_users?q='+ q;
      $.getScript(url);
    else
      if $('input:checked').length
        $.getScript($('input:checked').val());

  $(document).on 'change', 'input[type=checkbox]', ->
    $('input[type="checkbox"]').not(this).prop('checked', false)
    if this.checked
      console.log $(this).val();
      url = $(this).val();
      if $('input#q').val()
        url += '&q=' + $('input#q').val();
      $.getScript(url);
    else
      if $('input#q').val()
        $.getScript('/admin_users?q='+$('input#q').val());
      else
        $.getScript('admin_users')
