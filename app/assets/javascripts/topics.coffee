# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'ready page:load', ->
  ######$('div.pagination').hide()

  if $('div.forum_post').length < 10
    $('button#more_forum_posts').hide()

  $(document).on 'click','button#more_forum_posts', ->
    url = $('div.pagination .next_page').attr('href')
    if url
      $.getScript(url)
    else
      $(this).hide()

  $(document).on 'click', 'span#dlt_f_post', ->
    console.log $(this).attr('data-url');
    if confirm('Are you sure?')
      $.ajax({
        url: $(this).attr('data-url'),
        type: 'DELETE',
        dataType: 'script'
      })
