# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'ready page:load', ->
  maxchar = 300
  $('.forum_post').each ->
    content = $(this).children('div.content').html()
    if content.length > maxchar
      c = content.substr(0, maxchar)
      h = content.substr(maxchar, content.length)
      c = c + '<span class="less">...<a href="" class="morelink">more</a> </span>' + '<span class="more hidden">' + h + ' <a href="" class="lesslink">less</a> </span>'
      $(this).children('div.content').html c
    return
  $('.morelink').click ->
    $(this).parent().addClass 'hidden'
    $('.more').removeClass 'hidden'
    false
  $('.lesslink').click ->
    $(this).parent().addClass 'hidden'
    $('.less').removeClass 'hidden'
    false

