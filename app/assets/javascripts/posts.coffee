# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'ready page:load', ->
  $('div.pagination').hide()
  $('a.fancybox').fancybox({
    type: 'image'
  })

  showChar = 300
  ellipsestext = '...'
  $('.sidebar_post_content').each ->
    content = $(this).html()
    if content.length > showChar
      c = content.substr(0, showChar)
#      h = content.substr(showChar - 1, content.length - showChar)
      html = c + ellipsestext
      $(this).html html

  showChar = 450
  ellipsestext = '...'
  $('.post_content_collapsed').each ->
    content = $(this).html()
    if content.length > showChar
      c = content.substr(0, showChar)
      html = c + ellipsestext
      $(this).html html



  $(document).on 'click', '#more_button', ->
    url = $('div.pagination .next_page').attr('href')
    if url
      $.getScript(url)
    else
      $(this).hide()

  $(document).on 'click', 'i#dlt_comment', ->
    console.log 'Clicked'
    if confirm('Are you sure?')
      $.ajax({
        url: $(this).attr('data-url'),
        type: 'DELETE'
        dataType: 'script'
      })
