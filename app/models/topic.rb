class Topic < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :category
  has_many :forum_posts, dependent: :destroy

  validates_presence_of :name
  validates_presence_of :category_id

  scope :last_five, -> { order('created_at desc').limit(5)}
  scope :latest, -> { last(10) }

end
