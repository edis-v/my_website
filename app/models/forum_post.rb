class ForumPost < ActiveRecord::Base
  acts_as_votable
  belongs_to :user
  belongs_to :topic, counter_cache: true

  validates_presence_of :content

  def user_name
    self.user.name
  end


end
