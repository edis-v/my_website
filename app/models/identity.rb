class Identity < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :uid, :provider
  validates_uniqueness_of :uid, scope: :provider

  def self.find_for_oauth(auth)
    identity = find_or_create_by(uid: auth.uid, provider: auth.provider)
    #identity.accesstoken = auth.credentials.token
    identity.name = auth.info.name
    identity.email = auth.info.email
    identity.save
    identity
  end

  def first_name
    self.name.split(' ')[0]
  end

  def last_name
    self.name.split(' ')[1]
  end

end
