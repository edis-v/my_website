class Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :topics , dependent: :destroy# forum posts
  has_many :posts, dependent: :destroy

  validates_presence_of :name
  validates_presence_of :priority
  validates_uniqueness_of :priority

  scope :first_eight_by_priority, -> { order('priority asc').first(8) }
  scope :rest_by_priority, -> { offset(8).order('priority asc') }
=begin
  scope :with_popular_posts, -> { joins(:posts).
     where('posts.id = (SELECT MAX(posts.id) FROM posts WHERE posts.category_id = categories.id)').
      group('categories.id')

  }
=end

  def self.first_post(name)
    find_by_name(name).posts.first
  end

  def last_activity
    self.posts.last.created_at
  end

  def has_posts?
    self.posts_count > 0
  end

  def posts_desc
    self.posts.order('created_at desc')
  end

  def last_10_except_this(topic_id)
    self.topics.where.not(id: topic_id).last(10)
  end



end
