class Post < ActiveRecord::Base
  belongs_to :user, counter_cache: true
  belongs_to :category, counter_cache: true
  has_many :comments, dependent: :destroy
  has_many :images, dependent: :destroy

  accepts_nested_attributes_for :images

  scope :last_four, -> { order('created_at desc').limit(4).includes(:category, :images)}
  scope :popular, -> {order('comments_count desc').limit(4).includes(:category, :images)}
  scope :with_first_image, -> { joins(:images).
                  where('images.id = (SELECT MIN(images.id) FROM images where images.post_id = posts.id)').
                               group('posts.id')
  }

  validates_presence_of :category_id
  validates_presence_of :title
  validates_presence_of :content


  def save_image

  end

  def has_comments?
    comments.present?
  end

  def num_of_comments
    self.comments.size
  end

  def introduction
    "#{self.content.first(50)}..."
  end

  def large_introduction
    "#{self.content.first(160)}..."
  end

  def main_introduction
    "#{self.content.first(500)}..."
  end

end
