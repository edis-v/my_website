class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :post, counter_cache: true

  validates_presence_of :content

  def user_name
    self.user.name
  end

end
