class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :omniauthable

  has_many :posts, dependent: :nullify
  has_many :comments, dependent: :nullify
  has_many :forum_posts, dependent: :nullify
  has_many :identities, dependent: :destroy

  scope :moderators, -> { where(role: 1)}
  #scope :find_by_first_or_last_name, -> { |q| where('first_name like ? or last_name like ?', "%#{q}%","%#{q}%") }

  validates_presence_of :first_name, :last_name, :email, :password
  validates_format_of :email, with: Devise::email_regexp

  enum role: [:guest, :moderator, :admin]

  def name
    "#{first_name} #{last_name}"
  end

  def to_s
    puts "#{name} - #{email} - #{password} - #{encrypted_password}"
  end

  def posts_with_category
     self.posts.includes(:category)
  end

  def self.find_by_first_or_last_name(q)
    where('first_name like ? or last_name like ?', "%#{q}%","%#{q}%")
  end


end
