class ForumPostPolicy < ApplicationPolicy

  def new?
    user.admin? || user.moderator? || user.guest?
  end

  def create?
    user.admin? || user.moderator? || user.guest?
  end

  def edit?
    user.admin? || user.moderator?
  end

  def update?
    user.admin? || user.moderator?
  end

  def destroy?
    user.admin?
  end

  def upvote?
    user.admin? || user.moderator? || (user.guest? && record.user_id != user.id)
  end

  def downvote?
    user.admin? || user.moderator? || (user.guest? && record.user_id != user.id)
  end

end