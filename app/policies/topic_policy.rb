class TopicPolicy < ApplicationPolicy

  def new?
    user.admin? || user.moderator? || user.guest?
  end

  def create?
    user.admin? || user.moderator? || user.guest?
  end

  def edit?
    user.admin? || user.moderator?
  end

  def update?
    user.admin? || user.moderator?
  end

  def destroy?
    user.admin? || user.moderator?
  end

end