class CommentPolicy < ApplicationPolicy

  def new?
    user.admin?
  end

  def create?
    user.admin? || user.moderator? || user.guest?
  end

  def edit?
    user.admin?
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin? || user.moderator?
  end

end