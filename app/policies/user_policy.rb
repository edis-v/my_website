class UserPolicy < ApplicationPolicy

  def show?
    record.moderator?
  end

  def new?
    user.admin?
  end

  def create?
    user.admin?
  end

  def edit?
    user.admin?
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin?
  end

  def permitted_attributes
    if user.admin?
      [:email, :first_name, :last_name, :password, :role]
    else
      [:email, :first_name, :last_name, :password]
    end
  end


end