module ApplicationHelper

  def create_href(url)
    'http://res.cloudinary.com/dn2youjr9/image/upload/c_fill,h_385,w_775/'+url
  end

  def set_class?(slug)
    request.original_fullpath.include?(slug) ? 'active' : ''
  end

  def set_class_if_root_page
    current_page?(root_path) ? 'active' : ''
  end


end
