class UsersController < ApplicationController
  before_action :authenticate_user!, except: :show
  before_action :set_user, except: [:index, :new, :create]

  def index
    @users = User.all
    @users = @users.where(role: params[:role]) if params[:role].present?
    @users = @users.find_by_first_or_last_name(params[:q]) if params[:q].present?
    @users = @users.paginate(page: params[:page], per_page: 10)
    respond_to do |format|
        format.html { }
        format.js {}
    end
  end

  def show
    authorize @user
  end

  def new
    authorize User
    @user = User.new
  end

  def create
    @user = User.new
    @user.update(permitted_attributes(@user))
    authorize @user
    if  @user.save
      redirect_to admin_users_path, notice: 'User created successfully'
    else
      render 'new'
    end
  end

  def edit
    authorize @user
  end

  def update
    authorize @user
    if @user.update(permitted_attributes(@user))
      redirect_to admin_users_path, notice: 'New user created successfully'
    else
      render 'edit'
    end
  end

  def destroy
    authorize @user
    @user.destroy
    redirect_to admin_users_path, notice: 'User deleted successfully'
  end

  private

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :password, :role)
  end

  def set_user
    @user = User.find(params[:id])
  end

end
