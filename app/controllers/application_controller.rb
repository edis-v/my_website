class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  protect_from_forgery with: :exception
  before_action :detect_browser
  before_action :check_rack_mini_profiler
  before_action :categories_list


  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized


  private

  def categories_list
    @categories_list ||= Category.order('priority asc')
  end

  def detect_browser
    case request.user_agent
      when /iPad/i
        request.variant = :tablet
      when /iPhone/i
        request.variant = :phone
      when /Android/i && /mobile/i
        request.variant = :phone
      when /Android/i
        request.variant = :tablet
      when /Windows Phone/i
        request.variant = :phone
      else
        request.variant = :desktop
    end
  end

  def check_rack_mini_profiler
    # for example - if current_user.admin?
    if Rails.env.development?
      Rack::MiniProfiler.authorize_request
    elsif params[:rmp] && user_signed_in? && current_user.admin?
      Rack::MiniProfiler.authorize_request
    end
  end

  def user_not_authorized
    flash[:warning] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end

end
