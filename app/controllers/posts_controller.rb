class PostsController < ApplicationController
  before_action :authenticate_user!, except: :show
  before_action :set_category, except: [:new, :create]
  before_action :set_post, only: [:edit, :update, :destroy]


  def show
    @post = @category.posts.find(params[:id])
    @comments = @post.comments.includes(:user).paginate(page: params[:page], per_page: 5)
  end

  def new
    authorize Post
    @post = current_user.posts.build
    @image = @post.images.build
  end

  def create
    @post = current_user.posts.build(post_params)
    authorize @post
    if @post.save
      if params[:images].present?
        params[:images].each do |image|
          Image.create(image: image.to_s, post_id: @post.id)
        end
      end
      redirect_to category_post_path(@post.category,@post), notice: 'Post successfully published'
    else
      render 'new'
    end
  end

  def edit
    authorize @post
  end

  def update
    authorize @post
    if @post.update(post_params)
      redirect_to category_post_path(@post.category,@post), notice: 'Post successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    authorize @post
    @post.destroy
    redirect_to category_path(@post.category), notice: 'Post deleted successfully'
  end

  private

  def post_params
    params.require(:post).permit(:title, :content, :category_id, {images: []})
  end

  def set_post
    @post = @category.posts.find(params[:id])
  end

  def set_category
    @category = Category.friendly.find(params[:category_id])
  end
end
