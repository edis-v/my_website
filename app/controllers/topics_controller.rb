class TopicsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_topic, only: [:show, :edit, :update, :destroy]

  def index
    if params[:category_id].present?
      @category = Category.find(params[:category_id])
      @topics = @category.topics.paginate(page: params[:page], per_page: 10)
    else
      #@topics = Topic.order('forum_posts_count desc').includes(:category).paginate(page: params[:page], per_page: 5)
      @topics = Topic.order('forum_posts_count desc').joins(:category).select('topics.*, categories.name as category_name').paginate(page: params[:page], per_page: 10)
    end
  end

  def show
    @forum_posts = @topic.forum_posts.includes(:user).paginate(page: params[:page], per_page: 10)
  end

  def new
    authorize Topic
    @topic = Topic.new
  end

  def create
    @topic = Topic.new(topic_params)
    authorize @topic
    if @topic.save
      redirect_to @topic, notice: 'Successfully created new topic.'
    else
      render 'new'
    end
  end

  def edit
    authorize @topic
  end

  def update
    authorize @topic
    if @topic.update(topic_params)
      redirect_to @topic, notice: 'Topic successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    authorize @topic
    @topic.destroy
    redirect_to topics_path, notice: 'Topic deleted'
  end

  private

  def set_topic
    @topic = Topic.friendly.find(params[:id])
  end

  def topic_params
    params.require(:topic).permit(:name, :category_id)
  end

end
