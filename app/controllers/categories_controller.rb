class CategoriesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.all.includes(:posts)
  end

  def show
    @posts = @category.posts_desc.paginate(:page => params[:page], :per_page => 5)
  end
  
  def new
    authorize Category
    @category = Category.new
  end
  
  def create
    @category = Category.new(category_params)
    @category.priority = Category.maximum(:priority) + 1
    authorize @category
    respond_to do |format|
      if @category.save
        format.html {redirect_to @category, notice: 'Successfully added new category'}
        format.js {  }
      else
        render 'new'
      end
    end
  end

  def edit
    authorize @category
  end

  def update
    authorize @category
    if @category.update(category_params)
      redirect_to @category, notice: 'Category successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    authorize @category
    @category.destroy
    redirect_to categories_path, notice: 'Category deleted'
  end

  def sort
    params[:order].each do |key, value|
      Category.friendly.find(value[:id]).update_attribute(:priority,value[:position])
    end
    render nothing: true
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end

  def set_category
    @category = Category.friendly.find(params[:id])
  end
end
