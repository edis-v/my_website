class ForumPostsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_topic
  before_action :set_forum_post, only: [:destroy, :upvote, :downvote]


  def create
    authorize ForumPost
    @forum_post =  @topic.forum_posts.build(forum_post_params)
    @forum_post.user_id = current_user.id
    respond_to do |format|
      if @forum_post.save
        format.js { }
      else
        format.js { render 'create_with_error' }
      end
    end
  end

  def destroy
    authorize @forum_post
    @forum_post.destroy
    respond_to do |format|
      format.html { redirect_to @topic }
      format.js { }
    end
  end

  def upvote
    authorize @forum_post
    @forum_post.liked_by current_user
    redirect_to :back
  end

  def downvote
    authorize @forum_post
    @forum_post.downvote_from current_user
    redirect_to :back
  end

  private

  def forum_post_params
    params.require(:forum_post).permit(:content)
  end

  def set_forum_post
    @forum_post = @topic.forum_posts.find(params[:id])
  end

  def find_topic
    @topic = Topic.friendly.find(params[:topic_id])
  end
end
