class HomesController < ApplicationController

  def index
    posts_ids = []
    categories = Category.order('priority asc').limit(6).includes(:posts)
    categories.each do |c|
      posts_ids << c.posts.last.id
    end
    @posts = Post.where(id: posts_ids).includes(:images,:category)
    @topics = Topic.order('forum_posts_count desc').limit(5).joins(:category).select('topics.*, categories.name as category_name')#includes(:category)
  end

  def search
    @posts = Post.where('title like ?', "%_#{params[:query].to_s[1..-1]}%").paginate(:page => params[:page], :per_page => 5)
    @topics = Topic.where('name like ?', "%_#{params[:query].to_s[1..-1]}%").paginate(:page => params[:page], :per_page => 5)
  end



end
